package com.example.ratingService.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ratingService.entity.Hotel;
import com.example.ratingService.entity.Rating;
import com.example.ratingService.repository.HotelRepository;
import com.example.ratingService.repository.RatingRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class RatingServiceImpl implements RatingService {

	@Autowired
	private RatingRepository ratingRepository;

	@Override
	public List<Rating> getrRatingByUserId(String id) {
		// TODO Auto-generated method stub
		return ratingRepository.findByUserid(id);
	}

	@Override
	public List<Rating> getrRatingByHotelId(String id) {
		// TODO Auto-generated method stub

		return ratingRepository.findByHotelid(id);
	}

	@Override
	public Rating create(Rating rating) {
		String randomUserId = UUID.randomUUID().toString();
		rating.setRatingid(randomUserId);
		return ratingRepository.save(rating);
	}

	@Override
	public List<Rating> getALL() {
		// TODO Auto-generated method stub
		return ratingRepository.findAll();
	}

}
