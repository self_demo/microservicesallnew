package com.example.ratingService.services;

import java.util.List;

import com.example.ratingService.entity.Hotel;

public interface HotelService {
	// create
	Hotel create(Hotel hotel);

	// get all
	List<Hotel> getALL();

	// get single
	Hotel get(String id);

}
