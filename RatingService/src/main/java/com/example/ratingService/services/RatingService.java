package com.example.ratingService.services;

import java.util.List;

import com.example.ratingService.entity.Hotel;
import com.example.ratingService.entity.Rating;

public interface RatingService {
	// create
	Rating  create(Rating rating);

	// get all
	List<Rating > getALL();

	// get single
	List<Rating > getrRatingByUserId(String id);

	// get single
	List<Rating > getrRatingByHotelId(String id);

}
