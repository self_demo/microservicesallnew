package com.example.ratingService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ratingService.entity.Hotel;
import com.example.ratingService.entity.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, String> {

	List<Rating >  findByHotelid(String id);

	List<Rating >  findByUserid(String id);

}
