package com.example.ratingService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ratingService.entity.Hotel;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, String> {

}
