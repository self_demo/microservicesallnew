package com.example.ratingService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ratingService.entity.Hotel;
import com.example.ratingService.entity.Rating;
import com.example.ratingService.services.HotelService;
import com.example.ratingService.services.RatingService;

@RestController
@RequestMapping("/ratings")
public class RatingController {

	@Autowired
	private RatingService ratingService;

	@PostMapping
	public ResponseEntity<Rating> createUser(@RequestBody Rating user) {
		Rating user1 = ratingService.create(user);
		return ResponseEntity.status(HttpStatus.CREATED).body(user1);
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<List<Rating>> getSingleUser(@PathVariable String userId) {
		List<Rating> user = ratingService.getrRatingByUserId(userId);
		return ResponseEntity.ok(user);
	}

	@GetMapping("/hotel/{userId}")
	public ResponseEntity<List<Rating>> getSingleHotel(@PathVariable String userId) {
		List<Rating> user = ratingService.getrRatingByHotelId(userId);
		return ResponseEntity.ok(user);
	}

	// all user get
	@GetMapping
	public ResponseEntity<List<Rating>> getAllUser() {
		List<Rating> allUser = ratingService.getALL();
		return ResponseEntity.ok(allUser);
	}
}
