package com.example.hotelService.services;

import java.util.List;

import com.example.hotelService.entity.Hotel;

public interface HotelService {
	// create
	Hotel create(Hotel hotel);

	// get all
	List<Hotel> getALL();

	// get single
	Hotel get(String id);

}
