package com.example.hotelService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hotelService.entity.Hotel;
import com.example.hotelService.services.HotelService;

@RestController
@RequestMapping("/hotels")
public class HotelController {

	@Autowired
	private HotelService hotelService;

	@PostMapping
	public ResponseEntity<Hotel> createUser(@RequestBody Hotel user) {
		Hotel user1 = hotelService.create(user);
		return ResponseEntity.status(HttpStatus.CREATED).body(user1);
	}

	@GetMapping("/{userId}")
	public ResponseEntity<Hotel> getSingleUser(@PathVariable String userId) {
		Hotel user = hotelService.get(userId);
		return ResponseEntity.ok(user);
	}

	// all user get
	@GetMapping
	public ResponseEntity<List<Hotel>> getAllUser() {
		List<Hotel> allUser = hotelService.getALL();
		return ResponseEntity.ok(allUser);
	}
}
