package com.example.userService;

public class OddEvenGenerator {

	private int number = 1;

	private boolean evenFlag=false;
	private int numberTHread ;
	
	private int totalNumberInSequence ;

	public OddEvenGenerator(int numberTHread, int totalNumberInSequence) {
		super();
		this.numberTHread = numberTHread;
		this.totalNumberInSequence = totalNumberInSequence;
	}
	
	public void printOddNumber() {
		synchronized (this) {
			while (number< totalNumberInSequence) {
				if (evenFlag) {
					try {
						wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName()+" "+number++);
				evenFlag=true;
				notify();
				
			}
		}
	}
	
	public void printEvenNumber() {
		synchronized (this) {
			while (number< totalNumberInSequence) {
				if (!evenFlag) {
					try {
						wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName()+" "+number++);
				evenFlag=false;
				notify();
				
			}
		}
	}
	
	
}
