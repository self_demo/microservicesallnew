package com.example.userService;

import java.util.concurrent.Semaphore;

public class OddEvenNumberGeneratorSemaphor {

	private Semaphore oddSemaphore = new Semaphore(1);

	private Semaphore evenSemaphore = new Semaphore(0);

	public void printOddNumber(int number) {
		try {
			oddSemaphore.acquire();
			System.err.println(Thread.currentThread().getName() + " " + number);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			evenSemaphore.release();
		}
	}

	public void printEvenNumber(int number) {
		try {
			evenSemaphore.acquire();
			System.err.println(Thread.currentThread().getName() + " " + number);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			oddSemaphore.release();
		}
	}

}
