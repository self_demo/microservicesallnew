package com.example.userService;

public class OddEvenThread implements Runnable {

	private OddEvenGenerator oddEvenGenerator;
	private int result;

	public OddEvenThread(OddEvenGenerator oddEvenGenerator) {
		super();
		this.oddEvenGenerator=oddEvenGenerator;

	}

	@Override
	public void run() {
		oddEvenGenerator.printOddNumber();
	}

}
