package com.example.userService.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.userService.Exceptions.ResourceNotFoundException;
import com.example.userService.entity.Hotel;
import com.example.userService.entity.Rating;
import com.example.userService.entity.User;
import com.example.userService.external.services.HotelService;
import com.example.userService.repository.UserRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private HotelService hotelService;

	@Override
	public User saveUser(User user) {
		// generate unique user
		String randomUserId = UUID.randomUUID().toString();
		user.setUserId(randomUserId);
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUser() {
		return userRepository.findAll();
	}

	@Override
	public User getUser(String userId) {
		User orElseThrow = userRepository.findById(userId).orElseThrow(
				() -> new ResourceNotFoundException("User with given id is not found on server !! : " + userId));

//		 http://localhost:8083/ratings/user/c06430d2-f050-4d9c-b047-12ba83d5ed33

		Rating[] forObject = restTemplate.getForObject("http://RATING-SERVICE/ratings/user/" + orElseThrow.getUserId(),
				Rating[].class);

		List<Rating> list = Arrays.stream(forObject).toList();

		orElseThrow.setRatings(list);

		list.stream().map(ee -> {

//			ResponseEntity<Hotel> forEntity = restTemplate
//					.getForEntity("http://HOTEL-SERVICE/hotels/" + ee.getHotelid(), Hotel.class);
//			Hotel body = forEntity.getBody();

			Hotel body = hotelService.getHotel(ee.getHotelid());
			ee.setHotel(body);
			return ee;
		}).collect(Collectors.toList());

		System.err.println(forObject);

		return orElseThrow;
	}

}
