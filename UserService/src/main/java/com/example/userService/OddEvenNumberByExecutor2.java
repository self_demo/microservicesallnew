package com.example.userService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OddEvenNumberByExecutor2 {

	private static final int TOTAL_NUMBER_INSEQUENCE = 10;
	private static final int TOTAL_NUMBER_OFTHREADS = 2;

	public static void main(String[] args) {
		
		OddEvenGenerator oddEvenGenerator = new OddEvenGenerator(TOTAL_NUMBER_OFTHREADS, TOTAL_NUMBER_INSEQUENCE);

		ExecutorService executor = null;
		
			try {
				executor = Executors.newFixedThreadPool(TOTAL_NUMBER_OFTHREADS);
				executor.submit(new OddEvenThread(oddEvenGenerator));
				executor.submit(new EvenThread2(oddEvenGenerator));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				executor.shutdown();
			}
		
		
		
	}

}
