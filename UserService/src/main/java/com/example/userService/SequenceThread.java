package com.example.userService;

public class SequenceThread implements Runnable {

	private NumbersGenerator numbersGenerator;
	private int result;

	public SequenceThread(NumbersGenerator numbersGenerator, int result) {
		super();
		this.numbersGenerator = numbersGenerator;
		this.result = result;
	}

	@Override
	public void run() {
		numbersGenerator.printNumbers(result);
	}

}
