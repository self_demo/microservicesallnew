package com.example.userService;

public class OddEvenGeneratorSemaphorMain {

	public static void main(String[] args) {
		OddEvenNumberGeneratorSemaphor oddEvenNumberGeneratorSemaphor = new OddEvenNumberGeneratorSemaphor();

		Thread t1 = new Thread(new OddNumberGeneratorThread(oddEvenNumberGeneratorSemaphor, 10), "Thread-1");
		Thread t2 = new Thread(new EvenNumberGeneratorThread(oddEvenNumberGeneratorSemaphor, 10), "Thread-2");

		t1.start();

		t2.start();

	}

}
