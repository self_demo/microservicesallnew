package com.example.userService.external.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.userService.entity.Hotel;

@FeignClient(name = "HOTEL-SERVICE")
public interface HotelService {
	@GetMapping("/hotels/{hotelld}")
	Hotel getHotel(@PathVariable String hotelld);

}