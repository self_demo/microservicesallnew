package com.example.userService;

public class EvenNumberGeneratorThread implements Runnable {

	private OddEvenNumberGeneratorSemaphor oddAndEvenNumberGenerator;
	private int totalNumberInSEquence;

	public EvenNumberGeneratorThread(OddEvenNumberGeneratorSemaphor oddAndEvenNumberGenerator,
			int totalNumberInSEquence) {
		super();
		this.oddAndEvenNumberGenerator = oddAndEvenNumberGenerator;
		this.totalNumberInSEquence = totalNumberInSEquence;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 2; i <= totalNumberInSEquence; i = i + 2) {
			oddAndEvenNumberGenerator.printEvenNumber(i);
		}
	}

}
