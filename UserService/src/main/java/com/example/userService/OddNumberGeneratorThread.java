package com.example.userService;

public class OddNumberGeneratorThread implements Runnable {

	private OddEvenNumberGeneratorSemaphor oddAndEvenNumberGenerator;
	private int totalNumberInSEquence;

	public OddNumberGeneratorThread(OddEvenNumberGeneratorSemaphor oddAndEvenNumberGenerator,
			int totalNumberInSEquence) {
		super();
		this.oddAndEvenNumberGenerator = oddAndEvenNumberGenerator;
		this.totalNumberInSEquence = totalNumberInSEquence;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 1; i <= totalNumberInSEquence; i = i + 2) {
			oddAndEvenNumberGenerator.printOddNumber(i);
		}
	}

}
