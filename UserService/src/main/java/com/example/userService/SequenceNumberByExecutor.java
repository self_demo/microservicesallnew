package com.example.userService;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SequenceNumberByExecutor {

	private static final int TOTAL_NUMBER_INSEQUENCE = 10;
	private static final int TOTAL_NUMBER_OFTHREADS = 3;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumbersGenerator numbersGenerator = new NumbersGenerator(TOTAL_NUMBER_OFTHREADS, TOTAL_NUMBER_INSEQUENCE);

		ExecutorService executor = null;
		try {
			executor = Executors.newFixedThreadPool(TOTAL_NUMBER_OFTHREADS);

			executor.submit(new SequenceThread(numbersGenerator, 1));

			executor.submit(new SequenceThread(numbersGenerator, 2));

			executor.submit(new SequenceThread(numbersGenerator, 0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			executor.shutdown();
		}

	}

}
