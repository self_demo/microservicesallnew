package com.example.userService.Exceptions;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.userService.entity.Rating;

@FeignClient("RATING-SERVICE")
public interface RatingService {

	@PostMapping("/ratings")
	public Rating insert(Rating rating);

	
	@PostMapping("/ratings")
	public Rating update();

}
