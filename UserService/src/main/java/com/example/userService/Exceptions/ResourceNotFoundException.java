package com.example.userService.Exceptions;

public class ResourceNotFoundException extends RuntimeException {

	// extra properties that you want ma
	public ResourceNotFoundException() {
		super("Resource not found on server !!");
	}

	public ResourceNotFoundException(String message) {
		super(message);
	}
}
