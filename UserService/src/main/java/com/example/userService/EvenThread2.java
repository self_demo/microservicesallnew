package com.example.userService;

public class EvenThread2 implements Runnable {

	private OddEvenGenerator oddEvenGenerator;
	private int result;

	public EvenThread2(OddEvenGenerator oddEvenGenerator) {
		super();
		this.oddEvenGenerator=oddEvenGenerator;
	}

	@Override
	public void run() {
		oddEvenGenerator.printEvenNumber();;
	}

}
