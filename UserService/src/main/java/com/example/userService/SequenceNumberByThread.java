package com.example.userService;

public class SequenceNumberByThread {

	private static final int TOTAL_NUMBER_INSEQUENCE = 10;
	private static final int TOTAL_NUMBER_OFTHREADS = 3;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumbersGenerator numbersGenerator = new NumbersGenerator(TOTAL_NUMBER_OFTHREADS, TOTAL_NUMBER_INSEQUENCE);

		Thread t1=new Thread(new SequenceThread(numbersGenerator,1),"THread-1"); 
		Thread t2=new Thread(new SequenceThread(numbersGenerator,2),"THread-2"); 
		Thread t3=new Thread(new SequenceThread(numbersGenerator,0),"THread-3"); 
		
		t1.start();
		t2.start();
		t3.start();
		
	}

}
