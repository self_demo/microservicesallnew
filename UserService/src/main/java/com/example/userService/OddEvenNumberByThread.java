package com.example.userService;

public class OddEvenNumberByThread {

	private static final int TOTAL_NUMBER_INSEQUENCE = 10;
	private static final int TOTAL_NUMBER_OFTHREADS = 2;

	public static void main(String[] args) {
		
		OddEvenGenerator oddEvenGenerator = new OddEvenGenerator(TOTAL_NUMBER_OFTHREADS, TOTAL_NUMBER_INSEQUENCE);
		Thread t1=new Thread(new OddEvenThread(oddEvenGenerator),"THread-1"); 
		Thread t2=new Thread(new EvenThread2(oddEvenGenerator),"THread-2"); 
		
		t1.start();
		t2.start();
		
	}

}
