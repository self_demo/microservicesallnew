package com.example.userService;

public class NumbersGenerator {

	private int number = 1;

	private int numberTHread ;
	
	private int totalNumberInSequence ;

	public NumbersGenerator(int numberTHread, int totalNumberInSequence) {
		super();
		this.numberTHread = numberTHread;
		this.totalNumberInSequence = totalNumberInSequence;
	}
	
	public void printNumbers(int result) {
		synchronized (this) {
			while (number< totalNumberInSequence) {
				while (number % numberTHread != result) {
					try {
						wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName()+" "+number++);
				notifyAll();
				
			}
		}
	}
	
	
}
